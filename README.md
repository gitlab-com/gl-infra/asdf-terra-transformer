# Deprecated: use [`asdf-gl-infra`](https://gitlab.com/gitlab-com/gl-infra/asdf-gl-infra) instead.

This project has been depecated. Please use <https://gitlab.com/gitlab-com/gl-infra/asdf-gl-infra> instead.

---------------------------------------------------------------

# asdf-terra-transformer

As [asdf](https://github.com/asdf-vm/asdf) plugin for
[terra-transformer](https://gitlab.com/gitlab-com/gl-infra/terra-transformer).

## Installation

```
asdf plugin add terra-transformer https://gitlab.com/gitlab-com/gl-infra/terra-transformer.git
```

This plugin installs a binary named `terra-transformer`.

Requires:

* curl
* jq
